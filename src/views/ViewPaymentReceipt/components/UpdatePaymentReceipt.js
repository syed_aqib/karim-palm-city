import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Button,
  ModalFooter,
  Col,
  Row,
  Input,
  CardBody,
  Card,
  Label,
} from "reactstrap";

import validate from "../../../components/Utilities Component/ValidationWrapper";

const UpdatePaymentReceipt = (props) => {
  const {
    modal,
    toggle,
    // status,
    // complainId,
    // updateSupportStatus,
  } = props;

  // const [updateStatus, setUpdateStatus] = useState({
  //   supportStatusId: "",
  //   LastSupportStatusCommment: "",
  //   support_id: 0,
  // });
  // const [error, setError] = useState({
  //   supportStatusIdError: "",
  //   descriptionError: "",
  // });

  // const onSuccess = () => {
  //   swal("Congratulations!", "Updated successfully", "success");
  // };

  // const onFailure = () => {
  //   swal(
  //     "Sorry!",
  //     "Something Went Wrong Please Try Again Later or Contact Admin",
  //     "error"
  //   );
  // };

  // const handleSubmit = (e) => {
  //   e.preventDefault();
  //   if (
  //     updateStatus.supportStatusId !== "" &&
  //     updateStatus.LastSupportStatusCommment !== ""
  //   ) {
  //     updateSupportStatus(updateStatus, onSuccess, onFailure);
  //     close();
  //   }else{
  //     alert("Please fill all fields")
  //   }
  // };

  const close = () => {
    toggle();
    // reset();
  };

  // const reset = () => {
  //   setUpdateStatus({
  //     supportStatusId: "",
  //     LastSupportStatusCommment: "",
  //     support_id: 0,
  //   });
  // };

  // const CheckFields = (name) => {
  //   if (name === "required") {
  //     setError({
  //       ...error,
  //       supportStatusIdError: validate(
  //         "required",
  //         updateStatus.supportStatusId
  //       ),
  //     });
  //   } else if (name === "description") {
  //     setError({
  //       ...error,
  //       descriptionError: validate(
  //         "description",
  //         updateStatus.LastSupportStatusCommment
  //       ),
  //     });
  //   }
  // };

  // useEffect(() => {
  //   setUpdateStatus({ ...updateStatus, support_id: complainId });
  // }, [complainId]);

  return (
    <Modal size="md" isOpen={modal} toggle={toggle}>
      <ModalHeader toggle={toggle}>
        <h3>Update Receipt</h3>
      </ModalHeader>
      <Row>
        <div className="col">
          <Card className="shadow">
            <CardBody>
              <form>
                <ModalBody>
                  <div>
                    <Row>
                      <Col lg="6" md="6" sm="6" xs="12">
                        <label
                          className="form-control-label"
                          for="input-username"
                        >
                          Form No.
                        </label>
                        <input
                          //   value={location.state.sale_order_number}
                          // disabled
                          type="text"
                          id="input-username"
                          className="form-control mb-3"
                        ></input>
                      </Col>
                    </Row>
                    {/* <br /> */}
                    <hr style={{ margin: "0px" }} />
                    {/* <br /> */}
                    <Row className=" mt-3">
                      <Col lg="6" md="6" sm="6" xs="12">
                        <label
                          className="form-control-label"
                          for="input-username"
                        >
                          Payment Through.
                        </label>
                        <Input
                          type="select"
                          //   value={state.PaymentThroughId}
                          //   onChange={(e) => onChange(e.target.value, "PaymentThroughId")}
                        >
                          <option>Select Payment Option</option>
                          {/* {props.PaymentThrough !== null &&
                props.PaymentThrough !== undefined &&
                props.PaymentThrough.map((val) => {
                  return (
                    <option
                      key={val.PaymentThroughId}
                      value={val.PaymentThroughId}
                    >
                      {val.PaymentthroughName}
                    </option>
                  );
                })} */}
                        </Input>
                      </Col>
                      <Col lg="6" md="6" sm="6" xs="12">
                        <label
                          className="form-control-label"
                          for="input-username"
                        >
                          Amount
                        </label>
                        <input
                          type="text"
                          id="input-username"
                          className="form-control"
                          placeholder="Amount"
                          //   onChange={(e) => onChange(e.target.value, "Amount")}
                          //   value={setstate.PR_useramount}
                          //  disabled
                          // onBlur={() => CheckFields("Amount")}
                          onKeyPress={(event) => {
                            if (!/[0-9]/.test(event.key)) {
                              event.preventDefault();
                            }
                          }}
                        ></input>
                      </Col>
                      <Col lg="12" md="12" sm="6" xs="12">
                        <label
                          className="form-control-label"
                          for="input-username"
                        >
                          Remarks:
                        </label>
                        <Input
                          type="text"
                          className="form-control"
                          //    value={props.stateData.Remarks}
                          // onBlur={() => CheckFields("remarks")}
                          //   onChange={(e) => onChange(e.target.value, "remarks")}
                        ></Input>

                        <br />
                      </Col>
                    </Row>
                    {/* <Row>
                      <Col>
                        <Button
                          className="btn-icon btn-2"
                          color="success"
                          type="button"
                          id="save"
                          // onClick={handlePrint}
                        >
                          <span className="btn-inner--icon">
                            <i className="fas fa-save"></i>
                            Save
                          </span>
                        </Button>
                      </Col>
                    </Row> */}
                  </div>
                </ModalBody>
                <ModalFooter>
                  <Button type="submit" color="success">
                    Update
                  </Button>
                  <Button color="warning" onClick={close}>
                    Close
                  </Button>
                </ModalFooter>
              </form>
            </CardBody>
          </Card>
        </div>
      </Row>
    </Modal>
  );
};

export default UpdatePaymentReceipt;
