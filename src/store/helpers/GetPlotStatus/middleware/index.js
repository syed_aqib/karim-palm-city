import axios from "axios";
import {
  
  PlotStatusDetail,
  PlotInfoDetail,
 
} from "../action";
import {
  
  PLOTSTATUS_PATH,
  PLOTINFO_PATH,
 
} from "../constant";
import { BASEURL, URL } from "config/api/URL";




export const showPlotStatus = (OnSuccess, OnFailure) => (dispatch) => {
  dispatch(PlotStatusDetail.PlotStatus());
  let token = localStorage.getItem("auth");
  axios
    .get(`${URL}${PLOTSTATUS_PATH}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
      },
    })
    .then((res) => {
      if (res.data.status === true) {
        dispatch(PlotStatusDetail.PlotStatus_Success(res.data.response));
        dispatch(OnSuccess(res.data.message));
      } else {
        dispatch(PlotStatusDetail.PlotStatus_Failure(res.data.message));
        dispatch(OnFailure(res.data.message));
      }
    })
    .catch((error) => dispatch(PlotStatusDetail.PlotStatus_Failure(error)));
};
export const showPlotInfo = (body, OnSuccess, OnFailure) => (dispatch) => {

  dispatch(PlotInfoDetail.PlotInfo());
  let token = localStorage.getItem("auth")
  axios
    .post(`${URL}${PLOTINFO_PATH}`, body, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
      },
    })
    .then((res) => {
  
      if (res.data.status === true) {
        dispatch(PlotInfoDetail.PlotInfo_Success(res.data.response));
        dispatch(OnSuccess(res.data.message));
      } else {
        dispatch(PlotInfoDetail.PlotInfo_Failure(res.data.message));
        dispatch(OnFailure(res.data.message));
      }
    })

    .catch((error) => dispatch(PlotInfoDetail.PlotInfo_Failure(error)));
};
