/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import {
  useLocation,
  Route,
  Switch,
  Redirect,
  useHistory,
} from "react-router-dom";
// reactstrap components
import { Container } from "reactstrap";
// core components
import AdminNavbar from "components/Navbars/AdminNavbar/container/AdminNavbar";
import AdminFooter from "components/Footers/AdminFooter.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import routes from "routes/routes.js";
// import routesRep from "routes/routes-representative";
// import routesAccount from "routes/routes-account";

// import routesDoc from "routes/routes-documentation";

// import routesDev from "routes/routes-dev";

// import routesCustomerCounter from "routes/routes-customerCounter";
// import routesCashierCounter from "routes/routes-cashierCounter";
// import routesFormIssue from "routes/routes-formIssue";
// import routesVerificationCounter from "routes/routes-verificationCounter";
// import Register from "views/examples/Register.js";routesVerificationCounter

const Admin = (props) => {

  const [routesToRedner, setRoutesToRedner] = useState(null);
  const mainContent = React.useRef(null);
  const location = useLocation();




  useEffect(()=>{
    setRoutesToRedner(routes)
  },[])
 

  React.useEffect(() => {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    mainContent.current.scrollTop = 0;
  }, [location]);

  const getRoutes = (routes) => {
    if (routes !== false) {
      return routes.map((prop, key) => {
        if (prop.layout === "/admin") {
          return (
            <Route
              path={prop.layout + prop.path}
              component={prop.component}
              key={key}
            />
          );
        } else if (prop.subNav) {
          return prop.subNav.map((item, index) => {
            return (
              <Route
                path={item.layout + item.path}
                component={item.component}
                key={index}
              />
            );
          });
        }
      });
    }
  };

  const getBrandText = (path) => {
    for (let i = 0; i < routes.length; i++) {
      if (
        props.location.pathname.indexOf(routes[i].layout + routes[i].path) !==
        -1
      ) {
        return routes[i].name;
      }
    }
    return "Dashboard";
  };
  // const [routes, setroutes] = useState('initialState')

  return (
    <>
      <Sidebar
        {...props}
        routes={
          routes
       
        }
        logo={{
          innerLink: "/admin/index",
          imgSrc: require("../assets/img/square-pro-1.png").default,
        }}
      />

      <div className="main-content" ref={mainContent}>
        <AdminNavbar
          {...props}
          // brandText={getBrandText(props.location.pathname)}
        />
        <Switch>
          {getRoutes(
            routesToRedner !== null &&
              routesToRedner !== undefined &&
              routesToRedner
          )}
          {/* <Redirect from="*" to="/auth/login" /> */}
        </Switch>
        <Container>
          <AdminFooter />
        </Container>
      </div>
    </>
  );
};

export default Admin;
