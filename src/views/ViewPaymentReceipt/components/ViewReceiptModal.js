import React from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Button,
  ModalFooter,
  Col,
  Row,
} from "reactstrap";
import "react-h5-audio-player/lib/styles.css";
const ViewReceiptModal = ({ toggle, modal, viewReceipt }) => {
  const dateFunction = (date) => {
    const nDate = new Date(date).toLocaleString("en-US", {
      timeZone: "Asia/Karachi",
    });
    return nDate;
  };

  return (
    <Modal size="lg" isOpen={modal} toggle={toggle}>
      <ModalHeader toggle={toggle}>
        <h3>Details</h3>
      </ModalHeader>
      <ModalBody>
        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Name:</label>
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.name}</label>{" "}
          </Col>
        </Row>
        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Father/Spouse Name:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.fatherName}</label>{" "}
          </Col>
        </Row>
        <Row>
          <Col lg="4" md="4" sm="6">
            <label>CNIC:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.cnic}</label>{" "}
          </Col>
        </Row>
        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Email:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.email}</label>{" "}
          </Col>
        </Row>
        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Phone:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.mobile}</label>{" "}
          </Col>
        </Row>

        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Date of birth:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>
              {dateFunction(viewReceipt.DOB).toLocaleString("en-US", {
                timeZone: "Asia/Karachi",
              })}
            </label>{" "}
          </Col>
        </Row>

        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Passport No:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.passportNo}</label>{" "}
          </Col>
        </Row>

        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Nationality:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.Nationality}</label>{" "}
          </Col>
        </Row>
        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Residential Address:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.address}</label>{" "}
          </Col>
        </Row>

        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Organization:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.organization}</label>{" "}
          </Col>
        </Row>
        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Project Name:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.Project?.title}</label>{" "}
          </Col>
        </Row>
        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Plot category:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.plot_category?.title}</label>{" "}
          </Col>
        </Row>
        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Plot Type:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.plot_type?.title}</label>{" "}
          </Col>
        </Row>
        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Quantity:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.plotQuantity}</label>{" "}
          </Col>
        </Row>

        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Payment Through:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            <label>{viewReceipt.receipts?.[0].paymentThrough?.title}</label>{" "}
          </Col>
        </Row>

        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Payment Reference:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            {" "}
            <label>{viewReceipt.receipts?.[0].PaymentReferenceNo}</label>{" "}
          </Col>
        </Row>
        <Row>
          <Col lg="4" md="4" sm="6">
            <label>Amount:</label>{" "}
          </Col>
          <Col lg="6" md="6" sm="6">
            {" "}
            <label>{viewReceipt.receipts?.[0].amount}</label>{" "}
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={toggle}>
          Close
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ViewReceiptModal;
