import axios from "axios";
import { UserLogin } from "../action";
import { LOGIN_PATH } from "../constant";
import { BASEURL } from "config/api/URL";

export const CheckLogin = (body, OnSuccess, OnFailure) => (dispatch) => {
  console.log(BASEURL);
  dispatch(UserLogin.Login());
  let token = localStorage.getItem("auth");
  axios
    .post(`${BASEURL}${LOGIN_PATH}`, body, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `bearer ${token}`,
      },
    })
    .then((res) => {
      if (res.status === 200) {
        dispatch(UserLogin.LoginSuccess(res.data.response));
        dispatch(OnSuccess(res.data.message));
      } else {
        dispatch(UserLogin.LoginFailure(res.data.message));
        dispatch(OnFailure(res.data.message));
      }
    })
    .catch((error) => console.log(error));
};
