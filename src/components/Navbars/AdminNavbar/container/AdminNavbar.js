import React, { useState, useRef, useEffect } from "react";
import { Link } from "react-router-dom";
import logo from "../../../../assets/img/square-pro-1.png";
import { connect } from "react-redux";
import { CheckNotification, UpdateNotificationStatus } from "../middleware";
import axios from "axios";

// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  FormGroup,
  Badge,
  // InputGroupAddon,
  // InputGroupText,
  // Input,
  // InputGroup,
  Navbar,
  Nav,
  Container,
  Media,
} from "reactstrap";
import NotificationAlert from "react-notification-alert";
import { useHistory } from "react-router-dom";
// import axios from "axios";

// import swal from 'sweetalert';

const AdminNavbar = (props) => {
  let history = useHistory();

  const removeData = () => {
    // localStorage.removeItem('token');
    localStorage.removeItem("roleid");
    localStorage.removeItem("teamid");
    localStorage.removeItem("auth");
    // location.href = ("../");
  };
  if (
    localStorage.teamid === undefined &&
    localStorage.roleid === undefined &&
    localStorage.auth === undefined
  ) {
    let path = "/auth/login";
    history.push(path);
  }
  let localStore = parseInt(localStorage.teamid);

  useEffect(() => {
    props.CheckNotification(Body, onSuccess, onFailure);
  }, [true]);

  const onSuccess = () => {};
  const onFailure = () => {
    // window.alert("Fail");
  };

  const [data, setData] = useState(null);
  const [count, setCount] = useState(0);
  const [Body, setBodyAPI] = useState({
    id: "",
    Taskid: "",
    Datetime: "",
    status_id: "",
    Note: "",
    Meetingdatetime: "",
    orderstatus: "",
    logtype: "",
    CallOutcome: "",
    Dashboarduserid: "",
    teamid: localStore,
    Agentname: "",
    status_name: "",
  });
 

  var style = {
    boxShadow:
      "3px 3px 3px 3px rgba(0, 0, 0, 0.16), 3px 3px 3px 3px rgba(0, 0, 0, 0.16)",
    borderRadius: "3%",
    overflowY: "scroll",
    height: "350px",
    padding: "10px",
    marginRight: "290px",
  };

  var style1 = {
    boxShadow:
      "3px 3px 3px 3px rgba(0, 0, 0, 0.08), 3px 3px 3px 3px rgba(0, 0, 0, 0.16)",
    borderRadius: "3%",
    // padding: '5px'
  };
  const handleNotification = () => {
    props.CheckNotification(Body, onSuccess, onFailure);
  };
  const handleChangeStatus = (id) => {
    let body = {
      NotifyId: id,
    };
    props.UpdateNotificationStatus(body, onSuccess, onFailure);
  };

  return (
    <>
      <Navbar className="navbar-top navbar-dark" expand="md" id="navbar-main">
        <Container fluid>
          <Link
            className="h4 mb-0 text-black bold-lg text-uppercase d-none d-lg-inline-block"
            to="/admin/dashboard"
          >
            {props.brandText}
          </Link>
          {/* <Link style={{marginBottom:'-30px', marginLeft:'-150px'}}>
          <img src={logo} style={{ marginRight:900,marginTop:40, height:50}}></img> </Link> */}
          <Form className="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
            <FormGroup className="mb-0">
              {/* <InputGroup className="input-group-alternative">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fas fa-search" />
                  </InputGroupText>
                </InputGroupAddon>
                 <Input placeholder="Search" type="text" />
              </InputGroup> */}
            </FormGroup>
          </Form>
          <Nav className="align-items-center d-none d-md-flex" navbar>
           
            <UncontrolledDropdown nav>
              {/* <DropdownToggle nav>
                <Media className="align-items-center"></Media>
              </DropdownToggle> */}
              <DropdownToggle className="pr-0" nav>
                <Media className="align-items-center">
                  <span className="avatar avatar-sm rounded-circle">
                    <img
                      alt="..."
                      src={require("../../../../assets/img/logo.jpg").default}
                    />
                  </span>
                  <Media className="ml-2 d-none d-lg-block">
                    <span className="mb-0 text-sm font-weight-bold"></span>
                  </Media>
                </Media>
              </DropdownToggle>
              <DropdownMenu className="dropdown-menu-arrow" right>
                {/* <DropdownItem className="noti-title" header tag="div">
                  <h6 className="text-overflow m-0">Welcome!</h6>
                </DropdownItem>
                <DropdownItem to="/admin/user-profile" tag={Link}>
                  <i className="ni ni-single-02" />
                  <span>My profile</span>
                </DropdownItem>
                <DropdownItem to="/admin/user-profile" tag={Link}>
                  <i className="ni ni-settings-gear-65" />
                  <span>Settings</span>
                </DropdownItem>
                <DropdownItem to="/admin/user-profile" tag={Link}>
                  <i className="ni ni-calendar-grid-58" />
                  <span>Activity</span>
                </DropdownItem>
                <DropdownItem to="/admin/user-profile" tag={Link}>
                  <i className="ni ni-support-16" />
                  <span>Support</span>
                </DropdownItem>
                <DropdownItem divider /> */}
                <DropdownItem onClick={removeData} href="/auth/login">
                  <i className="ni ni-user-run" />
                  <span onClick={removeData}>Logout</span>
                </DropdownItem>
                <DropdownItem href="/auth/forget">
                  <i className="ni ni-ni ni-lock-circle-open" />
                  <span>Forget Password</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
};

const mapStateToProps = (state) => ({
  // isLoading: state.getNotifications.isLoading,
  // isError: state.getNotifications.isError,
  // Data: state.getNotifications.Data,
  // Error: state.getNotifications.Error,
});
const mapDispatchToPrpos = (dispatch) => {
  //UpdateNotificationStatus
  return {
    CheckNotification: (body, onSuccess, OnFailure) =>
      dispatch(CheckNotification(body, onSuccess, OnFailure)),
    UpdateNotificationStatus: (body, onSuccess, OnFailure) =>
      dispatch(UpdateNotificationStatus(body, onSuccess, OnFailure)),
  };
};
export default connect(mapStateToProps, mapDispatchToPrpos)(AdminNavbar);
