export const GET_PAYMENT_THROUGH = "GET_PAYMENT_THROUGH";
export const GET_PAYMENT_THROUGH_SUCCESS = "GET_PAYMENT_THROUGH_SUCCESS";
export const GET_PAYMENT_THROUGH_FAILURE = "GET_PAYMENT_THROUGH_FAILURE";
export const GET_PAYMENT_THROUGH_PATH = "paymentThrough/getpaymentThrough";

export const CREATE_PAYMENT_RECEIPT = "CREATE_PAYMENT_RECEIPT";
export const CREATE_PAYMENT_RECEIPT_SUCCESS = "CREATE_PAYMENT_RECEIPT_SUCCESS";
export const CREATE_PAYMENT_RECEIPT_FAILURE = "CREATE_PAYMENT_RECEIPT_FAILURE";
export const CREATE_PAYMENT_RECEIPT_PATH = "reciept/createReciept";
