import { LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE } from "../constant.js";
export class UserLogin {
  static Login() {
    return {
      type: LOGIN,
    };
  }
  static LoginSuccess(response) {
    return {
      type: LOGIN_SUCCESS,
      payload: response,
    };
  }
  static LoginFailure(error) {
    return {
      type: LOGIN_FAILURE,
      error,
    };
  }
}
