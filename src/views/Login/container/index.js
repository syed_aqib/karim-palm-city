import { connect } from "react-redux";
import Login from "./Login";
import { CheckLogin } from "../middleware";
const mapStateToProps = (state) => ({
  isLoading: state.login.isLoading,
  isError: state.login.isError,
  Data: state.login.Data,
  Error: state.login.error,
});
const mapDispatchToPrpos = (dispatch) => {
  return {
    CheckUserLogin: (body, OnSuccess, OnFailure) =>
      dispatch(CheckLogin(body, OnSuccess, OnFailure)),
  };
};
export default connect(mapStateToProps, mapDispatchToPrpos)(Login);
