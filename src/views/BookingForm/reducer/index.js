import { CUSTOMER, CUSTOMER_SUCCESS, CUSTOMER_FAILURE,
  PLOTTYPE,
  PLOTTYPE_SUCCESS,
  PLOTTYPE_FAILURE,
  PLOTSIZE, PLOTSIZE_FAILURE, PLOTSIZE_SUCCESS,PLOTPROJECT,PLOTPROJECT_FAILURE,PLOTPROJECT_SUCCESS } from "../constant";
const INITIAL_STATE = {
  Data: null,
  isLoading: false,
  isError: false,
  error: {},
  Type: null,
  Size: null,
  Project:null,
};
export default (body = INITIAL_STATE, action) => {
  switch (action.type) {
    default:
      return body;
      case PLOTPROJECT:
        return {
          ...body,
          isLoading: true,
          isError: false,
  
          Project: null,
        };
      case PLOTPROJECT_SUCCESS:
        return {
          ...body,
          isLoading: false,
  
          Project: action.payload,
        };
      case PLOTPROJECT_FAILURE:
        return {
          ...body,
  
          isError: true,
          error: action.error,
        };
    case CUSTOMER:
      return {
        ...body,
        isLoading: true,
        isError: false,

        Data: null,
      };
    case CUSTOMER_SUCCESS:
      return {
        ...body,
        isLoading: false,

        Data: action.payload,
      };
    case CUSTOMER_FAILURE:
      return {
        ...body,

        isError: true,
        error: action.error,
      };
      case PLOTTYPE:
        return {
          ...body,
          isLoading: true,
          isError: false,
          isLoggedIn: false,
          Type: null,
          error: {},
        };
      case PLOTTYPE_SUCCESS:
        return {
          ...body,
          isLoading: false,
          isLoggedIn: true,
          Type: action.payload,
        };
      case PLOTTYPE_FAILURE:
        return {
          ...body,
          isLoading: false,
          isError: true,
          error: action.error,
        };
      case PLOTSIZE:
        return {
          ...body,
          isLoading: true,
          isError: false,
          isLoggedIn: false,
          Size: null,
          error: {},
        };
      case PLOTSIZE_SUCCESS:
        return {
          ...body,
          isLoading: false,
          isLoggedIn: true,
          Size: action.payload,
        };
      case PLOTSIZE_FAILURE:
        return {
          ...body,
          isLoading: false,
          isError: true,
          error: action.error,
        };



  }
};
