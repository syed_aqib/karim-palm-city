// import Index from "views/Index.js";
import Login from "views/Login/container";
import BookingForm from "views/BookingForm/container";
import PaymentReceipt from "views/PaymentReceipt/container";
import ViewPaymentReceipt from "views/ViewPaymentReceipt/container";
import ViewCustomerDetail from "views/ViewCustomerDetail/container";

var routes = [
  {
    path: "/createPaymentReceipt",
    name: "Create Payment Receipt",
    icon: "fas fa-angle-right",
    component: PaymentReceipt,
    layout: "/admin",
    isView: false,
  },
  {
    name: "Form",
    icon: "fab fa-wpforms ",
    isView: true,
    subNav: [
      {
        path: "/createForm",
        name: "Create Form",
        icon: "fas fa-angle-right",
        component: BookingForm,
        layout: "/admin",
        isView: true,
      },
      {
        path: "/ViewCustomerDetail",
        name: "View Customer Detail",
        icon: "fas fa-angle-right",
        component: ViewCustomerDetail,
        layout: "/admin",
        isView: true,
      },
    ],
  },
  {
    name: "Payment Receipt",
    icon: "fas fa-file-invoice-dollar",
    isView: true,
    subNav: [
      {
        path: "/viewPaymentReceipt",
        name: "View Payment Receipt",
        icon: "fas fa-angle-right",
        component: ViewPaymentReceipt,
        layout: "/admin",
        // isView: false,
      },
    ],
  },
  {
    path: "/login",
    component: Login,
    layout: "/auth",
  },

  // {
  //   path: "/reset",
  //   component: ResetPassword,
  //   layout: "/auth",
  // },
];
export default routes;
